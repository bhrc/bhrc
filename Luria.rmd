---
title: "Luria"
author: "André Simioni"
date: "19 de março de 2018"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")

# Aditional librarys
library(lubridate); library(lavaan); library(broom)

# Load settings.r
source("settings.r", echo = T)
```

# Get data from RedCap
```{r}
luria_raw <- import(bhrc_noidraw, setclass = "tibble")%>%
  select(ident, redcap_event_name, p_ludp1:p_lupe8, -p_ludp1_ns) %>%
  filter(redcap_event_name == "wave0_arm_1")

# summary
luria_raw %>% sumfac(-1:-2, .label = T)
```

# Classic Scores
```{r}
luria <- luria_raw %>%
  na_if("") %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  rec(p_ludp2:p_lupe7, rec = "77,99=NA; else=copy", suffix = "")

# summary
luria %>% names()
luria %>% sumfac()
```

# Latent Variables
```{r}
dblv <- luria %>%
  select(ident, redcap_event_name, num_range("p_ludp",1:6), num_range("p_luo",1:6), num_range("p_lupe",1:6)) %>% 
  mutate(across(c(num_range("p_ludp",2:6), num_range("p_luo",2:6), num_range("p_lupe",2:6)), ordered)) %>%
  drop_na(-1:-2)
dblv %>% summary()

# Second-order CFA
model <- "
lu_tim_fl =~ p_ludp1 + p_luo1 + p_lupe1
lu_acc_fl =~ p_ludp2 + p_luo2 + p_lupe2
lu_flu_fl =~ p_ludp3 + p_luo3 + p_lupe3
lu_pre_fl =~ p_ludp4 + p_luo4 + p_lupe4
lu_sym_fl =~ p_ludp5 + p_luo5 + p_lupe5
lu_coo_fl =~ p_ludp6 + p_luo6 + p_lupe6
lu_mot_fl =~ lu_tim_fl + lu_acc_fl + lu_flu_fl + lu_pre_fl + lu_sym_fl + lu_coo_fl

#residual correlations for teh ludp task
p_ludp1 ~~ p_ludp2
p_ludp1 ~~ p_ludp3
p_ludp1 ~~ p_ludp4
p_ludp1 ~~ p_ludp5
p_ludp1 ~~ p_ludp6
p_ludp2 ~~ p_ludp3
p_ludp2 ~~ p_ludp4
p_ludp2 ~~ p_ludp5
p_ludp2 ~~ p_ludp6
p_ludp3 ~~ p_ludp4
p_ludp3 ~~ p_ludp5
p_ludp3 ~~ p_ludp6
p_ludp4 ~~ p_ludp5
p_ludp4 ~~ p_ludp6
p_ludp5 ~~ p_ludp6

#residual correlations for teh luo task
p_luo1 ~~ p_luo2
p_luo1 ~~ p_luo3
p_luo1 ~~ p_luo4
p_luo1 ~~ p_luo5
p_luo1 ~~ p_luo6
p_luo2 ~~ p_luo3
p_luo2 ~~ p_luo4
p_luo2 ~~ p_luo5
p_luo2 ~~ p_luo6
p_luo3 ~~ p_luo4
p_luo3 ~~ p_luo5
p_luo3 ~~ p_luo6
p_luo4 ~~ p_luo5
p_luo4 ~~ p_luo6
p_luo5 ~~ p_luo6

#residual correlations for teh lupe task
p_lupe1 ~~ p_lupe2
p_lupe1 ~~ p_lupe3
p_lupe1 ~~ p_lupe4
p_lupe1 ~~ p_lupe5
p_lupe1 ~~ p_lupe6
p_lupe2 ~~ p_lupe3
p_lupe2 ~~ p_lupe4
p_lupe2 ~~ p_lupe5
p_lupe2 ~~ p_lupe6
p_lupe3 ~~ p_lupe4
p_lupe3 ~~ p_lupe5
p_lupe3 ~~ p_lupe6
p_lupe4 ~~ p_lupe5
p_lupe4 ~~ p_lupe6
p_lupe5 ~~ p_lupe6
"
cfa_luria_HO_w0 <- cfa(model, data=dblv, estimator="WLSMV", std.lv=T, missing="pairwise") # pairwise not converge
cfa_luria_HO_w0 %>% summary(fit.measures=T)
facload <- cfa_luria_HO_w0 %>% lavPredict()  # Extract factor loadings
## Store factor loading in a identified dataframe
fs_luria_HO_w0 <- dblv %>% select(ident,redcap_event_name) %>% cbind(facload) %>% as_tibble()

# Correlated 6-factor model
model <-'
TIM =~ p_ludp1 + p_luo1 + p_lupe1
ACC =~ p_ludp2 + p_luo2 + p_lupe2
FLU =~ p_ludp3 + p_luo3 + p_lupe3
PRE =~ p_ludp4 + p_luo4 + p_lupe4
SYM =~ p_ludp5 + p_luo5 + p_lupe5
COO =~ p_ludp6 + p_luo6 + p_lupe6

# residual correlations for teh ludp task
p_ludp1 ~~ p_ludp2
p_ludp1 ~~ p_ludp3
p_ludp1 ~~ p_ludp4
p_ludp1 ~~ p_ludp5
p_ludp1 ~~ p_ludp6
p_ludp2 ~~ p_ludp3
p_ludp2 ~~ p_ludp4
p_ludp2 ~~ p_ludp5
p_ludp2 ~~ p_ludp6
p_ludp3 ~~ p_ludp4
p_ludp3 ~~ p_ludp5
p_ludp3 ~~ p_ludp6
p_ludp4 ~~ p_ludp5
p_ludp4 ~~ p_ludp6
p_ludp5 ~~ p_ludp6

#residual correlations for teh luo task
p_luo1 ~~ p_luo2
p_luo1 ~~ p_luo3
p_luo1 ~~ p_luo4
p_luo1 ~~ p_luo5
p_luo1 ~~ p_luo6
p_luo2 ~~ p_luo3
p_luo2 ~~ p_luo4
p_luo2 ~~ p_luo5
p_luo2 ~~ p_luo6
p_luo3 ~~ p_luo4
p_luo3 ~~ p_luo5
p_luo3 ~~ p_luo6
p_luo4 ~~ p_luo5
p_luo4 ~~ p_luo6
p_luo5 ~~ p_luo6

#residual correlations for teh lupe task
p_lupe1 ~~ p_lupe2
p_lupe1 ~~ p_lupe3
p_lupe1 ~~ p_lupe4
p_lupe1 ~~ p_lupe5
p_lupe1 ~~ p_lupe6
p_lupe2 ~~ p_lupe3
p_lupe2 ~~ p_lupe4
p_lupe2 ~~ p_lupe5
p_lupe2 ~~ p_lupe6
p_lupe3 ~~ p_lupe4
p_lupe3 ~~ p_lupe5
p_lupe3 ~~ p_lupe6
p_lupe4 ~~ p_lupe5
p_lupe4 ~~ p_lupe6
p_lupe5 ~~ p_lupe6
'
cfa_luria_cor6_w0 <- cfa(model, data=dblv, estimator="WLSMV", std.lv=T, missing="pairwise")
cfa_luria_cor6_w0 %>% summary(fit.measures=T)
facload <- cfa_luria_cor6_w0 %>% lavPredict()  # Extract factor loadings
# Store factor loading in a identified dataframe
fs_luria_cor6_w0 <- dblv %>% select(ident,redcap_event_name) %>% cbind(facload) %>% as.tibble()

# Best fits
lst(cfa_luria_HO_w0, cfa_luria_cor6_w0) %>% map_df(glance, .id = "id")
```

# Merge
```{r}
luria <- luria %>%
  left_join(fs_luria_HO_w0) %>% 
  #left_join(fs_luria_cor6_w0) %>% 
  copy_labels(luria_raw) %>%
  var_labels(lu_tim_fl="Luria: time fac. loading",
             lu_acc_fl="Luria: corrects fac. loading",
             lu_flu_fl="Luria: fluency fac. loading",
             lu_pre_fl="Luria: precision fac. loading",
             lu_sym_fl="Luria: symmetry fac. loading",
             lu_coo_fl="Luria: coordination fac. loading",
             lu_mot_fl="Luria: motor skills overall fac. loading")
```

# Export
```{r}
luria %>% export("bhrc_data/Proc/Luria.rds")
```