---
title: "Add fixed vars to covid protocol"
author: "Andre"
date: "11/11/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")

# Aditional librarys
library(lubridate)

# Load settings.r
source("settings.r", echo = T)
```

# Open data
Repetir
x- covid_suspect  1y
x- y_covidexp  1y
x- y_jobloss 1y
x- y_financeloss  1y
x- y_homestability
x- y_worryfood
x- y_governhelp
x- dcany da w2

```{r}
dawba <- read_rds("bhrc_data/Proc/dawbanet_df.rds") %>% 
  filter(redcap_event_name == "wave2_arm_1") %>% 
  select(ident, dcany)
dawba

covid <- read_rds("bhrc_data/Proc/Covid.rds") %>% 
  filter(redcap_event_name == "wave2_covid1_arm_1") %>% 
  select(ident, y_covidsuspected, y_covidexp, y_jobloss, y_financeloss, y_homestability, y_worry_food, y_governhelp) %>% 
  set_na(-1:-2, na = 77) %>% 
  rec(y_covidsuspected:y_covidexp, rec = "0=0; 1:3=1; else=copy", suffix = "") %>% 
  rec(y_homestability, rec = "0:1=0; 2:4=1; else=copy", suffix = "") %>% 
  set_labels(y_covidsuspected:y_covidexp, y_homestability, labels = c(No=0, Yes=1))
covid %>% sumfac(.label = T)

emacovid <- read_rds("bhrc_data/Proc/emacov.rds")
emacovid
```

# Merge
```{r}
final <- emacovid %>% 
  left_join(dawba) %>% 
  left_join(covid)
final %>% select(ident, redcap_event_name, dcany:y_governhelp)
```

# Export
```{r}
final %>% export("bhrc_data/Export/Export_data/emacovid_fixed.rds")
```

