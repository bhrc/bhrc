---
title: "Dom_Life_History_W1"
author: "André Rafael Simioni"
date: "14 de outubro de 2017"
output: html_document
editor_options: 
  chunk_output_type: console
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate); 

# Load settings.r
source("settings.r", echo = T)
```

# Get raw data
```{r}
lifehist_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1" & redcap_event_name != "wave0_arm_1") %>% 
  select(ident, redcap_event_name, 
         lf1:le20problems, # w1
         lf1_w2:le20problems_w2 # w2
         )

```

# Compute
```{r}
lifehist <- lifehist_raw %>% 
  set_na(lf1:lf12, le1:le20, na=99) %>% 
  set_na(ends_with("problems"), ends_with("problems_w2"), na = c(99,77)) %>% 
  rec(matches("(s1|s2|time)$"), matches("(s1|s2|time)_w2$"), rec = "99,77=0; 999=NA; else=copy", suffix = "") %>%
  # W1
  ## lf
  mutate(across(c(lf1allthetime, matches("^lf120.+(s1|s2)$")), ~if_else(lf1 == 0, 0, .)),
         across(c(lf2allthetime, matches("^lf220.+(s1|s2)$")), ~if_else(lf2 == 0, 0, .)),
         across(c(lf3allthetime, matches("^lf320.+(s1|s2)$")), ~if_else(lf3 == 0, 0, .)),
         across(c(lf4allthetime, matches("^lf420.+(s1|s2)$")), ~if_else(lf4 == 0, 0, .)),
         across(c(lf5allthetime, matches("^lf520.+(s1|s2)$")), ~if_else(lf5 == 0, 0, .)),
         across(c(lf6allthetime, matches("^lf620.+(s1|s2)$")), ~if_else(lf6 == 0, 0, .)),
         across(c(lf7allthetime, matches("^lf720.+(s1|s2)$")), ~if_else(lf7 == 0, 0, .)),
         across(c(lf8allthetime, matches("^lf820.+(s1|s2)$")), ~if_else(lf8 == 0, 0, .)),
         across(c(lf9allthetime, matches("^lf920.+(s1|s2)$")), ~if_else(lf9 == 0, 0, .)),
         across(c(lf10allthetime, matches("^lf1020.+(s1|s2)$")), ~if_else(lf10 == 0, 0, .)),
         across(c(lf11allthetime, matches("^lf1120.+(s1|s2)$")), ~if_else(lf11 == 0, 0, .)),
         across(c(lf12allthetime, matches("^lf1220.+(s1|s2)$")), ~if_else(lf12 == 0, 0, .)),
  ## le
         across(c(matches("^le120.+(s1|s2)$"), le1problems), ~if_else(le1 == 0, 0L, as.integer(.))),
         across(c(matches("^le220.+(s1|s2)$"), le2problems), ~if_else(le2 == 0, 0L, as.integer(.))),
         across(c(matches("^le320.+(s1|s2)$"), le3problems), ~if_else(le3 == 0, 0L, as.integer(.))),
         across(c(matches("^le420.+(s1|s2)$"), le4problems), ~if_else(le4 == 0, 0L, as.integer(.))),
         across(c(matches("^le520.+(s1|s2)$"), le5problems), ~if_else(le5 == 0, 0L, as.integer(.))),
         across(c(matches("^le620.+(s1|s2)$"), le6problems), ~if_else(le6 == 0, 0L, as.integer(.))),
         across(c(matches("^le720.+(s1|s2)$"), le7problems), ~if_else(le7 == 0, 0L, as.integer(.))),
         across(c(matches("^le820.+(s1|s2)$"), le8problems), ~if_else(le8 == 0, 0L, as.integer(.))),
         across(c(matches("^le920.+(s1|s2)$"), le9problems), ~if_else(le9 == 0, 0L, as.integer(.))),
         across(c(matches("^le1020.+(s1|s2)$"), le10problems), ~if_else(le10 == 0, 0L, as.integer(.))),
         across(c(matches("^le1120.+(s1|s2)$"), le11problems), ~if_else(le11 == 0, 0L, as.integer(.))),
         across(c(matches("^le1220.+(s1|s2)$"), le12problems), ~if_else(le12 == 0, 0L, as.integer(.))),
         across(c(matches("^le1320.+(s1|s2)$"), le13problems), ~if_else(le13 == 0, 0L, as.integer(.))),
         across(c(matches("^le1420.+(s1|s2)$"), le14problems), ~if_else(le14 == 0, 0L, as.integer(.))),
         across(c(matches("^le1520.+(s1|s2)$"), le15problems), ~if_else(le15 == 0, 0L, as.integer(.))),
         across(c(matches("^le1620.+(s1|s2)$"), le16problems), ~if_else(le16 == 0, 0L, as.integer(.))),
         across(c(matches("^le1720.+(s1|s2)$"), le17problems), ~if_else(le17 == 0, 0L, as.integer(.))),
         across(c(matches("^le1820.+(s1|s2)$"), le18problems), ~if_else(le18 == 0, 0L, as.integer(.))),
         across(c(matches("^le1920.+(s1|s2)$"), le19problems), ~if_else(le19 == 0, 0L, as.integer(.))),
         across(c(matches("^le2020.+(s1|s2)$"), le20problems), ~if_else(le20 == 0, 0L, as.integer(.))),
  # W2
  ## lf
         across(c(lf1allthetime_w2, matches("^lf120.+(s1|s2)_w2$")), ~if_else(lf1_w2 == 0, 0, .)),
         across(c(lf2allthetime_w2, matches("^lf220.+(s1|s2)_w2$")), ~if_else(lf2_w2 == 0, 0, .)),
         across(c(lf3allthetime_w2, matches("^lf320.+(s1|s2)_w2$")), ~if_else(lf3_w2 == 0, 0, .)),
         across(c(lf4allthetime_w2, matches("^lf420.+(s1|s2)_w2$")), ~if_else(lf4_w2 == 0, 0, .)),
         across(c(lf5allthetime_w2, matches("^lf520.+(s1|s2)_w2$")), ~if_else(lf5_w2 == 0, 0, .)),
         across(c(lf7allthetime_w2, matches("^lf720.+(s1|s2)_w2$")), ~if_else(lf7_w2 == 0, 0, .)),
         across(c(lf8allthetime_w2, matches("^lf820.+(s1|s2)_w2$")), ~if_else(lf8_w2 == 0, 0, .)),
         across(c(lf9allthetime_w2, matches("^lf920.+(s1|s2)_w2$")), ~if_else(lf9_w2 == 0, 0, .)),
         across(c(lf10allthetime_w2, matches("^lf1020.+(s1|s2)_w2$")), ~if_else(lf10_w2 == 0L, 0, .)),
         across(c(lf11allthetime_w2, matches("^lf1120.+(s1|s2)_w2$")), ~if_else(lf11_w2 == 0L, 0, .)),
         across(c(lf12allthetime_w2, matches("^lf1220.+(s1|s2)_w2$")), ~if_else(lf12_w2 == 0L, 0, .)),
  ## le
         across(c(matches("^le120.+(s1|s2)_w2$"), le1problems_w2), ~if_else(le1_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le220.+(s1|s2)_w2$"), le2problems_w2), ~if_else(le2_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le320.+(s1|s2)_w2$"), le3problems_w2), ~if_else(le3_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le420.+(s1|s2)_w2$"), le4problems_w2), ~if_else(le4_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le520.+(s1|s2)_w2$"), le5problems_w2), ~if_else(le5_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le620.+(s1|s2)_w2$"), le6problems_w2), ~if_else(le6_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le720.+(s1|s2)_w2$"), le7problems_w2), ~if_else(le7_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le820.+(s1|s2)_w2$"), le8problems_w2), ~if_else(le8_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le920.+(s1|s2)_w2$"), le9problems_w2), ~if_else(le9_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1020.+(s1|s2)_w2$"), le10problems_w2), ~if_else(le10_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1120.+(s1|s2)_w2$"), le11problems_w2), ~if_else(le11_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1220.+(s1|s2)_w2$"), le12problems_w2), ~if_else(le12_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1320.+(s1|s2)_w2$"), le13problems_w2), ~if_else(le13_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1420.+(s1|s2)_w2$"), le14problems_w2), ~if_else(le14_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1520.+(s1|s2)_w2$"), le15problems_w2), ~if_else(le15_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1620.+(s1|s2)_w2$"), le16problems_w2), ~if_else(le16_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1720.+(s1|s2)_w2$"), le17problems_w2), ~if_else(le17_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1820.+(s1|s2)_w2$"), le18problems_w2), ~if_else(le18_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le1920.+(s1|s2)_w2$"), le19problems_w2), ~if_else(le19_w2 == 0, 0L, as.integer(.))),
         across(c(matches("^le2020.+(s1|s2)_w2$"), le20problems_w2), ~if_else(le20_w2 == 0, 0L, as.integer(.)))) %>% 
  # unite
  ## lf
  unite(lf1_unite, lf1, lf1_w2, sep = "", na.rm=T) %>%
  unite(lf2_unite, lf2, lf2_w2, sep = "", na.rm=T) %>% 
  unite(lf3_unite, lf3, lf3_w2, sep = "", na.rm=T) %>% 
  unite(lf4_unite, lf4, lf4_w2, sep = "", na.rm=T) %>% 
  unite(lf5_unite, lf5, lf5_w2, sep = "", na.rm=T) %>% 
  unite(lf6_unite, lf6, lf6_w2, sep = "", na.rm=T) %>% 
  unite(lf7_unite, lf7, lf7_w2, sep = "", na.rm=T) %>% 
  unite(lf8_unite, lf8, lf8_w2, sep = "", na.rm=T) %>% 
  unite(lf9_unite, lf9, lf9_w2, sep = "", na.rm=T) %>% 
  unite(lf10_unite, lf10, lf10_w2, sep = "", na.rm=T) %>% 
  unite(lf11_unite, lf11, lf11_w2, sep = "", na.rm=T) %>% 
  unite(lf12_unite, lf12, lf12_w2, sep = "", na.rm=T) %>% 
  ## le
  unite(le1_unite, le1, le1_w2, sep = "", na.rm=T) %>% 
  unite(le2_unite, le2, le2_w2, sep = "", na.rm=T) %>%
  unite(le3_unite, le3, le3_w2, sep = "", na.rm=T) %>% 
  unite(le4_unite, le4, le4_w2, sep = "", na.rm=T) %>% 
  unite(le5_unite, le5, le5_w2, sep = "", na.rm=T) %>% 
  unite(le6_unite, le6, le6_w2, sep = "", na.rm=T) %>% 
  unite(le7_unite, le7, le7_w2, sep = "", na.rm=T) %>% 
  unite(le8_unite, le8, le8_w2, sep = "", na.rm=T) %>% 
  unite(le9_unite, le9, le9_w2, sep = "", na.rm=T) %>% 
  unite(le10_unite, le10, le10_w2, sep = "", na.rm=T) %>% 
  unite(le11_unite, le11, le11_w2, sep = "", na.rm=T) %>% 
  unite(le12_unite, le12, le12_w2, sep = "", na.rm=T) %>% 
  unite(le13_unite, le13, le13_w2, sep = "", na.rm=T) %>% 
  unite(le14_unite, le14, le14_w2, sep = "", na.rm=T) %>% 
  unite(le15_unite, le15, le15_w2, sep = "", na.rm=T) %>% 
  unite(le16_unite, le16, le16_w2, sep = "", na.rm=T) %>% 
  unite(le17_unite, le17, le17_w2, sep = "", na.rm=T) %>% 
  unite(le18_unite, le18, le18_w2, sep = "", na.rm=T) %>% 
  unite(le19_unite, le19, le19_w2, sep = "", na.rm=T) %>% 
  unite(le20_unite, le20, le20_w2, sep = "", na.rm=T) %>% 
  mutate(across(ends_with("_unite"), ~na_if(., "") %>% as.integer)) %>% 
  # set val/var labels
  copy_labels(lifehist_raw)

lifehist %>% names()
lifehist %>% select(ends_with("unite"))
```

```{r}
# function
extract_label <- function(df, ...){
  labels <- df %>% select(...) %>% get_label() %>% as.list()
  names_new <- labels %>% names() %>% str_c(., "_unite")
  labels_new <- labels %>% 
    set_names(names_new) %>% 
    modify(~str_replace(., "From 2010 to 2014", "In the last 4y"))
}
nl <- extract_label(lifehist_raw, num_range("le", 1:20))

# modify labels
for (i in names(nl)) {
  lifehist[,i] <- set_label(lifehist[,i], nl[i])
}
lifehist$le1_unite %>% get_label()
```

# Export
```{r}
lifehist %>% export("bhrc_data/Proc/Lifehistory.rds")
```

