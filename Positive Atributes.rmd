---
title: "Untitled"
author: "André Simioni"
date: "18/01/2020"
output: html_document
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(MplusAutomation);

# Load settings.r
source("settings.r", echo = T)
```

# Get data from Redcap
## W0W1
```{r}
# Open data from W0 and remove NULL vars
dawbanet_raw_W0 <- import("~/Documentos/pCloud/Projetos/Analises/INPD/Data/dawbanet_w0.sav",setclass="tibble") %>% 
  add_column(redcap_event_name="wave0_arm_1",.after="subjectid"); dawbanet_raw_W0 %>% length()

notnull <- dawbanet_raw_W0 %>% map(~summary(is.na(.))) %>% map("TRUE") %>% as.matrix() %>% as.data.frame()%>% rownames_to_column() %>% as_tibble() %>% 
  mutate(V1=as.character(V1)) %>% filter(V1!=2511) %$% .$rowname # identify not null variables

dawbanet_raw_W0 <- dawbanet_raw_W0 %>% select(notnull,-id,-selection,-weights,-age,-gender,-ratername,-ratedate,-ident); dawbanet_raw_W0 %>% length()

# Open data from W1 and remove NULL vars
dawbanet_raw_W1 <- import("~/Documentos/pCloud/Projetos/Analises/INPD/Data/dawbanet_w1.sav",setclass="tibble")%>% 
  add_column(redcap_event_name="wave1_arm_1",.after="subjectid"); dawbanet_raw_W1 %>% length()

notnull <- dawbanet_raw_W1 %>% map(~summary(is.na(.))) %>% map("TRUE") %>% as.matrix() %>% as.data.frame()%>% rownames_to_column() %>% as.tibble() %>% 
  mutate(V1=as.character(V1)) %>% filter(V1!=2010) %$% .$rowname # identify not null variables

dawbanet_raw_W1 <- dawbanet_raw_W1 %>% select(notnull,-age,-gender,-ratername,-ratedate); dawbanet_raw_W1 %>% length()

# Select vars of interest
ysi_w0 <- dawbanet_raw_W0 %>% select(subjectid, redcap_event_name, starts_with("p1n1"), starts_with("p1n2"))
ysi_w0 %>% sumfac()
ysi_w1 <- dawbanet_raw_W1 %>% select(subjectid, redcap_event_name, starts_with("p1n1"), starts_with("p1n2"))
ysi_w1 %>% sumfac()
```
## W2
```{r}
# Open data
dawbakey_w2 <- import("~/Documentos/pCloud/Projetos/Analises/INPD/Data/Raw/W2/dawba_keys_w2.xlsx", setclass = "tibble") %>% 
  rename(subjectid = ID, dawbaid = DawbaID)

ysi_w2_raw <- import("~/Documentos/pCloud/Projetos/Analises/INPD/Data/Raw/W2/dawbanet/positive_atributes.sav", setclass = "tibble") %>% 
  rename(dawbaid = dawbaID)
ysi_w2_raw %>% names
ysi_w2_raw$sn1a %>% summary.factor()

# Missing rows
anti_join(ysi_w2_raw, dawbakey_w2) %>% pull(dawbaid)

# Merge keys
ysi_w2 <- left_join(dawbakey_w2, ysi_w2_raw) %>% 
  add_column(redcap_event_name = "wave2_arm_1", .after="subjectid") %>% 
  select(subjectid, redcap_event_name, starts_with("p1n1"), starts_with("p1n2")) %>% 
  set_na(-1:-2, na = -2)# %>% 
  filter_at(-1:-2, any_vars(complete.cases(.)))
ysi_w2 %>% names()
ysi_w2 %>% sumfac()
ysi_w2 %>% select(matches("n1"))
ysi_w2$p1n1a %>% summary()
```
# Compute Youth Strength Inventory (YSI)
As Hoffmann, M. S., Leibenluft, E., Stringaris, A., Laporte, P. P., Pan, P. M., Gadelha, A., … Salum, G. A. (2016). Positive attributes buffer the negative associations between low intelligence and high psychopathology with educational outcomes. Journal of the American Academy of Child and Adolescent Psychiatry, 55(1), 47–53. https://doi.org/10.1016/j.jaac.2015.10.013
## W0
```{r}
# Set Data
ysi_w0_lav <- ysi_w0 %>% 
  select(subjectid,redcap_event_name,p1n1a:p1n2l) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-subjectid,-redcap_event_name),ordered)
ysi_w0_lav %>% summary(); ysi_w0_lav %>% nrow()

# Psitive attributes model as Mauricio
model <- "# Latent Vars Definition
ysi_fl =~ p1n1a + p1n1b + p1n1c + p1n1d + p1n1e + p1n1f + p1n1g + p1n1h + p1n1i + p1n1j + p1n1k + p1n1l + p1n2a + p1n2b + p1n2c + p1n2d + p1n2e + p1n2f + p1n2g + p1n2h + p1n2i + p1n2j + p1n2k + p1n2l

# Fixing parameters
p1n2g ~~ p1n1c
p1n2g ~~ p1n2c
p1n2g ~~ p1n2d
p1n1g ~~ p1n1b
p1n1h ~~ p1n1c
p1n2c ~~ p1n1c
p1n1i ~~ p1n1d
p1n1i ~~ p1n1a
p1n2j ~~ p1n2a
p1n2l ~~ p1n2h
p1n1d ~~ p1n1a
p1n2d ~~ p1n2c
p1n2j ~~ p1n2c"

fit_ysi_w0 <-  cfa(model, data=ysi_w0_lav, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_ysi_w0 %>% summary(fit.measures=T, standardized=T)
#fit_ysi_w0 %>% lavInspect("zero.cell.tables")
#fit_ysi_w0 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagrama
#fit_ysi_w0 %>% reliability() # Reliability from the model - omega
facload <- fit_ysi_w0 %>% predict %>% as.data.frame(); facload %>% dim() # Extract factor loadings

# Store results in a dataframe
ysi_w0_fl <- ysi_w0_lav %>% select(subjectid, redcap_event_name) %>% bind_cols(facload)
ysi_w0_fl %>% skim()
rm(model, facload)

```

## W1
```{r}
# Set Data
ysi_w1_lav <- ysi_w1 %>% 
  select(subjectid,redcap_event_name,p1n1a:p1n2l) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-subjectid,-redcap_event_name),ordered)
ysi_w1_lav %>% summary(); ysi_w1_lav %>% nrow()

# Psitive attributes model as Mauricio
model <- "# Latent Vars Definition
ysi_fl =~ p1n1a + p1n1b + p1n1c + p1n1d + p1n1e + p1n1f + p1n1g + p1n1h + p1n1i + p1n1j + p1n1k + p1n1l + p1n2a + p1n2b + p1n2c + p1n2d + p1n2e + p1n2f + p1n2g + p1n2h + p1n2i + p1n2j + p1n2k + p1n2l

# Fixing parameters
p1n2g ~~ p1n1c
p1n2g ~~ p1n2c
p1n2g ~~ p1n2d
p1n1g ~~ p1n1b
p1n1h ~~ p1n1c
p1n2c ~~ p1n1c
p1n1i ~~ p1n1d
p1n1i ~~ p1n1a
p1n2j ~~ p1n2a
p1n2l ~~ p1n2h
p1n1d ~~ p1n1a
p1n2d ~~ p1n2c
p1n2j ~~ p1n2c"

fit_ysi_w1 <-  cfa(model, data=ysi_w1_lav, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_ysi_w1 %>% summary(fit.measures=T, standardized=T)
#fit_ysi_w1 %>% lavInspect("zero.cell.tables")
#fit_ysi_w1 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagrama
#fit_ysi_w1 %>% reliability() # Reliability from the model - omega
facload <- fit_ysi_w1 %>% predict %>% as.data.frame(); facload %>% dim() # Extract factor loadings

# Store results in a dataframe
ysi_w1_fl <- ysi_w1_lav %>% select(subjectid, redcap_event_name) %>% bind_cols(facload)
ysi_w1_fl %>% skim()
rm(model, facload)
```

## W2
```{r}
# Set Data
ysi_w2_lav <- ysi_w2 %>% 
  select(subjectid,redcap_event_name,p1n1a:p1n2l) %>% 
  filter_at(-1:-2, any_vars(complete.cases(.))) %>% 
  mutate_at(vars(-subjectid,-redcap_event_name),ordered)
ysi_w2_lav %>% summary(); ysi_w2_lav %>% nrow()

  # Psitive attributes model as Mauricio
model <- "# Latent Vars Definition
ysi_fl =~ p1n1a + p1n1b + p1n1c + p1n1d + p1n1e + p1n1f + p1n1g + p1n1h + p1n1i + p1n1j + p1n1k + p1n1l + p1n2a + p1n2b + p1n2c + p1n2d + p1n2e + p1n2f + p1n2g + p1n2h + p1n2i + p1n2j + p1n2k + p1n2l

# Fixing parameters
p1n2g ~~ p1n1c
p1n2g ~~ p1n2c
p1n2g ~~ p1n2d
p1n1g ~~ p1n1b
p1n1h ~~ p1n1c
p1n2c ~~ p1n1c
p1n1i ~~ p1n1d
p1n1i ~~ p1n1a
p1n2j ~~ p1n2a
p1n2l ~~ p1n2h
p1n1d ~~ p1n1a
p1n2d ~~ p1n2c
p1n2j ~~ p1n2c"

fit_ysi_w2 <-  cfa(model, data=ysi_w2_lav, estimator="WLSMV", std.lv=T, missing="pairwise")
fit_ysi_w2 %>% summary(fit.measures=T, standardized=T)
#fit_ysi_w2 %>% lavInspect("zero.cell.tables")
#fit_ysi_w2 %>% semPaths(what="paths", intercepts=F, residuals=F, sizeLat = 6, sizeMan = 4, sizeMan2=4) # Diagrama
#fit_ysi_w2 %>% reliability() # Reliability from the model - omega
facload <- fit_ysi_w2 %>% predict %>% as.data.frame(); facload %>% dim() # Extract factor loadings

# Store results in a dataframe
ysi_w2_fl <- ysi_w2_lav %>% select(subjectid, redcap_event_name) %>% bind_cols(facload)
ysi_w2_fl %>% skim()
rm(model, facload)
```

## Merge
```{r}
ysi <- lst(ysi_w0, ysi_w1) #, ysi_w2
ysi_fl <- lst(ysi_w0_fl, ysi_w1_fl) # , ysi_w2_fl

ysi <- map2_dfr(ysi, ysi_fl, left_join) %>%
  bind_rows(ysi_w2) %>% 
  arrange(subjectid, redcap_event_name) %>% 
  # Var / Val labels
  copy_labels(dawbanet_raw_W0) %>% 
  var_labels(ysi_fl = "YSI: Positive Atributes Fac. Load.") %>% 
  set_labels(-1:-2, labels = c(No = 0, "A little" = 1, "A lot" = 2))
  
ysi
```
# Export
```{r}
ysi %>% export("~/Documentos/pCloud/Projetos/Analises/INPD/Data/Proc/Positive_atributes.rds")
```

