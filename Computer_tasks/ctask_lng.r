# Make_lng_computertask

# Librarys and settings
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")

## Aditional librarys
library(lubridate)

## Load settings.r
source("settings.r", echo = T)

# Open data
agng_lng <- import("bhrc_data/Proc/agng_lng.rds", setclass = "tibble")
cmstr_lng <- import("bhrc_data/Proc/cmstr_lng.rds", setclass = "tibble")
g2cr_lng <- import("bhrc_data/Proc/g2cr_lng.rds", setclass = "tibble")
hta400_lng <- import("bhrc_data/Proc/hta400_lng.rds", setclass = "tibble")
kdots_lng <- import("bhrc_data/Proc/kdots_lng.rds", setclass = "tibble")


# Merge
ctask_lng <- agng_lng %>%
  full_join(cmstr_lng) %>%
  full_join(g2cr_lng) %>%
  full_join(hta400_lng) %>%
  full_join(kdots_lng)

# Export
ctask_lng %>% export("bhrc_data/Proc/ctask_lng.rds")