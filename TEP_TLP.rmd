---
title: "TEP and TLP"
author: "André Rafael Simioni"
date: "27 de Jameiro de 2021"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate); #library(lavaan); library(semPlot); library(ltm)

# Load settings.r
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```

# Get data from redcap
```{r}
bhrc_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name != "screening_arm_1" & redcap_event_name != "wave0_arm_1" &  redcap_event_name != "wave1_arm_1") %>% 
  select(ident, redcap_event_name, starts_with("p_tep"), starts_with("p_tlp"))
bhrc_raw
```

# Teste de escrita de palavras e pseudopalavras (TEPP)
```{r}
teptlp <- bhrc_raw %>% 
  select(ident, redcap_event_name, starts_with("p_tep"), starts_with("p_tlp")) %>% 
  set_na(starts_with("p_tep"), starts_with("p_tlp"), na = 99)

teptlp %>% sumfac()
teptlp %>% get_labels()
```

# Export
```{r}
teptlp %>% export("bhrc_data/Proc/teptlp.rds")
```

