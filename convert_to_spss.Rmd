---
title: "Convert to SPSS"
author: "Andre"
date: "02/12/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")

# Aditional librarys
library(lubridate)

# Load settings.r
source("settings.r", echo = T)
```

# Open data
```{r}
data <- read_rds("bhrc_data/Export/Export_data/Simone_BHRC_2021_11_13.rds")

data2 <- data %>% 
  mutate(across(c(where(is.integer), where(is.numeric)), as.numeric)) %>% 
  copy_labels(data)

haven::write_sav(data, "Simone_BHRC_2021_11_13.sav")
zip::zipr()
```

# Zip
```{r}
file <- "bhrc_data/Export/Export_data/Simone"
file.ext <- str_c(file, "_BHRC_",str_replace_all(today(),"-","_"), ".rds")
file.ext

# RDS
exp %>% export(file.ext, compress=T)
file.remove(file.ext)

# SAV
exp %>% haven::write_sav(file.ext)

# Zip
#file.zip <- str_c(file.ext, ".zip")
file.zip <- "Simone_BHRC_2021_11_13.zip"
zip(zipfile=file.zip, file=file.ext)
file.remove(file.ext)
```

