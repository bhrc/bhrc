---
title: "HRC Bender"
author: "André Simioni"
date: "17 de dezembro de 2017"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)

relabel <- function(df, ..., .suffix="", .prefix=""){
  # scope vars
  poke <- tidyselect::vars_select(names(df), ...)
  changein <- poke %>% str_c(.suffix)
  # label
  newlabel <- df %>% select(all_of(poke)) %>% get_label() %>% as_vector() %>% str_remove(" \\(.*\\)") %>% str_c(.prefix,.)
  df[,changein] <- set_label(df[,changein], label = newlabel)
  # output
  df
}
```

# Getting Data From Redcap
## W0W1
```{r}
bender_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  select(ident, redcap_event_name, p_bda:p_bd8) %>% 
  filter(redcap_event_name == "wave0_arm_1")
bender_raw %>% sumfac()
```

# Recode and Compute
```{r}
bender <- bender_raw %>%
  rec(p_bda:p_bd8, rec = "88=0; 77,99=NA; else=copy", suffix = "") %>%  # "Does not know/Did not answer"=99, "Refuses"=77
  row_sums(p_bda:p_bd8, var = "p_bd_sum", n = 0.9) %>% 
  set_labels(p_bda:p_bd8, labels = c("0 points"=0, "1 point"=1, "2 points"=2)) %>% 
  add_labels(p_bd6, labels = c("3 points"=3)) %>% 
  var_labels(p_bd_sum = "Bender: Summed score") %>% 
  relabel(p_bda:p_bd8, .prefix = "Bender: ")

bender %>% sumfac(-1:-2, .label = T)
bender %>% get_label()
```

# Export
```{r}
bender %>% export("bhrc_data/Proc/Bender.rds")
```

