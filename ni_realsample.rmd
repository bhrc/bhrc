---
title: "Neuroimage sample"
author: "Andre"
date: "22/01/2022"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate); library(googlesheets4)

# Load settings.r
setwd("/home/andresimi/Documentos/pCloud/Projetos/Analises/bhrc/")
source("settings.r", echo = T)
```

# Open files
```{r}
files <- list.files("bhrc_data/Raw/neuroimage/zugman_doneni/", full.names = T, pattern = "T1w") %>% 
  c(list.files("bhrc_data/Raw/neuroimage/zugman_doneni/", full.names = T, pattern = "bold"))
files

zugman <- import_list(files, setclass = "tibble", header=F) %>% 
  imap(~transmute(.x, ident = str_remove(V1, "sub-") %>% as.numeric,
                      redcap_event_name = case_when(str_detect(.y, "_w0_") ~ "wave0_arm_1",
                                                    str_detect(.y, "_w1_") ~ "wave1_arm_1",
                                                    str_detect(.y, "_w2_") ~ "wave2_arm_1")))
zugman

ages <- read_rds("bhrc_data/Proc/Ages.rds") %>% select(ident, redcap_event_name, nc_date)
ages

freesurfert1w <- read_rds("bhrc_data/Proc/Neuroimage.rds") %>% select(ident, redcap_event_name, ni_lh_bankssts_area)
freesurfert1w
```

# zugman t1w vs bold
```{r}
zugman %>% map_dbl(nrow)

# W0
anti_join(zugman$ids_w0_T1w, zugman$ids_w0_bold) %>% nrow()
anti_join(zugman$ids_w0_bold, zugman$ids_w0_T1w) %>% nrow()

# W1
anti_join(zugman$ids_w1_T1w, zugman$ids_w1_bold) %>% nrow()
anti_join(zugman$ids_w1_bold, zugman$ids_w1_T1w) %>% nrow()

# W2
anti_join(zugman$ids_w2_T1w, zugman$ids_w2_bold) %>% nrow()
anti_join(zugman$ids_w2_bold, zugman$ids_w2_T1w) %>% nrow()
```

# Dont jave nc_date
```{r}
# freesurfer on´t have data
freesurfert1w %>% filter(is.na(ni_lh_bankssts_area))

# freesurfer that don't have nc date
freesurfert1wb <- left_join(freesurfert1w, ages)
freesurfert1wb
freesurfert1wb %>% filter(is.na(nc_date))

# Zugman files
zage <- zugman %>% map(left_join, ages)
zage %>% map(~filter(., is.na(nc_date)))

# W0 check if nc_date
anti_join(zage$ids_w0_T1w, zage$ids_w0_bold) %>% print(n=100)
anti_join(zage$ids_w0_bold, zage$ids_w0_T1w) %>% print(n=100)

# W1 check if nc_date
anti_join(zage$ids_w1_T1w, zage$ids_w1_bold) %>% print(n=100)
anti_join(zage$ids_w1_bold, zage$ids_w1_T1w) %>% print(n=100)

# W2 check if nc_date
anti_join(zage$ids_w2_T1w, zage$ids_w2_bold) %>% print(n=100)
anti_join(zage$ids_w2_bold, zage$ids_w2_T1w) %>% print(n=100)
```

# clipboard table to markdown
```{r}
library(clipr)
library(knitr)

readclip <- read_clip_tbl() %>% as_tibble()
readclip
readclip %>% kable()
```

|Name                                    |Type              |  W0|  W1|  W2|obs             |
|:---------------------------------------|:-----------------|---:|---:|---:|:---------------|
|with nc_date                            |T1w AND Bold      | 776| 488| 419|NA              |
|with Freesurfer data                    |Structural (T1W)  | 740| 466| 418|NA              |
|Zugman nifiti files (done neuroimage)   |Structural (T1W)  | 742| 466| 419|NA              |
|Zugman nifiti files (done neuroimage)   |Functional (Bold) | 685| 456| 422|NA              |
|Zugman nifiti files (done t1w not bold) |Structural (T1W)  |  58|  16|   5|NA              |
|Zugman nifiti files (done bold not t1w) |Functional (Bold) |   1|   6|   8|missing nc_date |
|**Zugman nifiti files FINAL Real N**    |T1w AND Bold      | 743| 472| 427|NA              |