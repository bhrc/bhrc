---
title: "Friends"
author: "André Simioni"
date: "08/08/2022"
output: html_document
editor_options: 
  chunk_output_type: console
---

# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(MplusAutomation);

# Load settings.r
source("settings.r", echo = T)
```

# Open data
```{r}
bhrc_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(!str_detect(redcap_event_name, "screening|covid")) %>% 
  select(ident, wave, starts_with("p1fr"), -p1friend)

bhrc_raw %>% sumfac()
bhrc_raw$p1friend %>% get_label()
```

# Latent model
## Create and Run model
Based on: 
Hoffmann MS, Leibenluft E, Stringaris A, et al. Positive attributes buffer the negative associations between low intelligence and high psychopathology with educational outcomes. Journal of the American Academy of Child and Adolescent Psychiatry. 2016;55(1):47-53. doi:10.1016/j.jaac.2015.10.013

```{r}
# prepare data
mdata <- bhrc_raw
mdata

mdata %>% names %>% nchar() %>% set_names(mdata %>% names)
mdata %>% sumfac(.max=10)

# Build model
model <- mplusObject(
   TITLE = "BHRC Friends;",
   VARIABLE = "IDVARIABLE = ident;
               CATEGORICAL = p1fr1-p1fr6;
               CLUSTER = wave;
               !WEIGHT = weight;",
   ANALYSIS = "TYPE = COMPLEX; !PARAMETERIZATION = THETA;",
   MODEL = "F1 BY p1fr1 p1fr2;
            F2 BY p1fr4 p1fr5 p1fr6;
            Friends BY F1 F2 p1fr3;",
   OUTPUT = "STDYX SAMPSTAT;",
   SAVEDATA = "FILE = friendsfs.dat; SAVE = FSCORES;",
   # PLOT = "PLOT is PLOT3;",
   rdata = mdata)

# Run model
fit <- mplusModeler(model,
                    dataout = here("MPLUS", "friends.dat"),
                    Mplus_command = "~/.cxoffice/mplus/drive_c/Program\ Files/Mplus/Mplus.exe", # comment it on windows
                    check = T, run = 1L, hashfilename = F, writeData = "always")
fit
```

## Read models
```{r}
# Warnings
output <- readModels(here("MPLUS", "friends.out"))
output$warnings
output$errors %>% simplify()
output$sampstat
output$summaries %>% as_tibble()
output$parameters
output$output

# Extract factor scores
factorscores <- output$savedata %>% as_tibble %>% 
  rename_with(tolower) %>% 
  transmute(ident, 
            wave,
            friends_fl = friends) %>% 
  var_labels(friends_fl = "Dawba Friends Fac. Load.")
factorscores

# Merge with obse data
friends <- left_join(mdata, factorscores) %>% 
  mutate(redcap_event_name = str_c("wave", wave, "_arm_1"), wave = NULL) %>% 
  relocate(redcap_event_name, .after = ident)

```

# Export
```{r}
friends %>% export("bhrc_data/Proc/Friends.rds")
```

