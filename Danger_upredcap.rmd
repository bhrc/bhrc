---
title: "Retrive previous data"
author: "André Simioni"
date: "02/05/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---
Retrive contacts data from redcap


# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(bit64)

# Load settings.r
source("settings.r", echo = T)
```

# Get data from redcap
```{r}
# Open data
bhrc_labelled <- import("bhrc_data/Raw/w0:2/bhrc_labelled.rds", setclass = "tibble") %>% 
  filter(str_detect(redcap_event_name, "screening_arm_1|covid", negate=T))


bhrc_labelled$reg50d %>% get_label()
```

# Functions
```{r}
last_wave <- function(x) {
  case_when(!is.na(nth(x, 3)) ~ nth(x, 3),
            is.na(nth(x, 3)) & !is.na(nth(x, 2)) ~ nth(x, 2),
            is.na(nth(x, 3)) & is.na(nth(x, 2)) & !is.na(nth(x, 1)) ~ nth(x, 1))
}
```

# Set data
```{r}
# set data
upredcap <- bhrc_labelled %>% 
  group_by(ident) %>% 
  transmute(ident,
            subjectid,
         redcap_event_name, 
         p_name1 = name_child, 
         p_gender = gender,
         p_reg9 = last_wave(reg9),
         p_name2 = name_mother,
         p_name3 = name_father,
         p_address_st = p_endereco,
         # Moving?
         p_address_st_new = last_wave(p_reg55a),
         p_reg55b = last_wave(p_reg55b),
         
         # Youth contacts
         p_reg11 = last_wave(reg11),
         p_reg12 = last_wave(reg12),
         p_nreg17 = last_wave(nreg17) %>% str_to_lower(),
         p_nreg18 = last_wave(nreg18), # facebook
         # Mother contacts
         p_reg23 = last_wave(reg23),
         p_reg25 = last_wave(reg25),
         p_reg25a = last_wave(reg25a),
         p_reg26 = last_wave(reg26),
         p_reg27 = last_wave(reg27),
         p_mother_address_st = last_wave(reg24a1),
         p_mother_address_n = last_wave(reg24a2),
         p_mother_address_dist = last_wave(reg24a3),
         p_mother_address_city = last_wave(reg24a4),
         p_mother_address_state = last_wave(reg24a5),
         p_mother_address_zip = last_wave(reg28a),

         # Father contacts
         p_nreg38 = last_wave(nreg38),
         p_reg40 = last_wave(reg40),
         p_reg40a = last_wave(reg40a),
         p_reg41 = last_wave(reg41),
         p_reg42 = last_wave(reg42),
         p_father_address_st = last_wave(reg39a1),
         p_father_address_n = last_wave(reg39a2),
         p_father_address_dist = last_wave(reg39a3),
         p_father_address_city = last_wave(reg39a4),
         p_father_address_state = last_wave(reg39a5),
         p_father_address_zip = last_wave(reg43a),
         
         # Other contacts
         p_reg50 = last_wave(reg50),
         p_reg50a = last_wave(reg50a),
         p_reg50b = last_wave(reg50b),
         p_n2reg50c = last_wave(n2reg50c),
         p_n2reg50d = last_wave(n2reg50d),
         p_reg51 = last_wave(reg51),
         p_reg51a = last_wave(reg51a),
         p_reg51b = last_wave(reg51b),
         p_n2reg51c = last_wave(n2reg51c),
         p_n2reg51d = last_wave(n2reg51d),
         p_reg52 = last_wave(reg52),
         p_reg52a = last_wave(reg52a),
         p_reg52b = last_wave(reg52b),
         p_n2reg52c = last_wave(n2reg52c),
         p_n2reg52d = last_wave(n2reg52d),
         p_reg53 = last_wave(reg53),
         p_reg53a = last_wave(reg53a),
         p_reg53b = last_wave(reg53b),
         p_n2reg53c = last_wave(n2reg53c),
         p_n2reg53d = last_wave(n2reg53d),
         #p_reg54 = last_wave(reg54),
         #p_reg54a = last_wave(reg54a),
         #p_reg54b = last_wave(reg54b),
         p_n2reg54c = last_wave(n2reg54c),
         p_n2reg54d = last_wave(n2reg54d),
         across(ends_with("_zip"), ~str_remove(., "-") %>% as.integer),
         across(ends_with("_zip"), ~if_else(.==1e+12, NA_integer_, .))
         ) %>% 
  ungroup() %>% 
  na_if("") %>% 
  rec(p_reg25, p_reg40, rec = "88=NA; else=copy", suffix = "") %>% 
  filter(redcap_event_name == "wave0_arm_1") %>% 
  select(-redcap_event_name)
bhrc_labelled$p_reg55b

bhrc_labelled %>% select(ident, redcap_event_name, p_reg51)
```

# Export
```{r}
upredcap %>% export("danger_redcap.csv")
```
