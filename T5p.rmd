---
title: "5 points test"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys
library(lubridate); library(forcats)

# Load settings.r
source("settings.r", echo = T)
```

# Open Raw data
```{r}
bhrc <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name == "wave0_arm_1") %>%
  select(ident, redcap_event_name, starts_with("p_t5p"))
bhrc %>% sumfac(.max = 50)
```

# Compute
```{r}
t5p <- bhrc
```

# Export 
```{r}
t5p %>% export("bhrc_data/Proc/T5p.rds")
```

