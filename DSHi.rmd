---
title: "DSHi"
author: "André Simioni"
date: "09/10/2020"
output: html_document
editor_options: 
  chunk_output_type: console
---
# Librarys and settings
First, read the README.md section about `settings.r` file.
```{r lib,  include=FALSE}
# Aditional librarys

# Load settings.r
setwd("~/Documentos/pCloud/Projetos/Analises/bhrc")
source("settings.r", echo = T)
```


# Get raw data
```{r}
dsh_raw <- import(bhrc_noidraw, setclass = "tibble") %>% 
  filter(redcap_event_name == "wave2_arm_1") %>% 
  select(ident, redcap_event_name, starts_with("p_dsh"))
dsh_raw %>% names
dsh_raw %>% sumfac()
```

# Compute
```{r}
cut <- 1

dsh <- dsh_raw %>% 
  na_if("") %>% 
  set_na(starts_with("p_dshi"), na = 99) %>%
  row_sums(p_dshi1:p_dshi9, p_dshi10b, p_dshi11, var = "p_dshi_sum", n=.8) %>%
  mutate(p_dshi_sc = if_else(p_dshi1 >= cut | p_dshi2 >= cut | p_dshi3 >= cut | p_dshi4 >= cut | p_dshi5 >= cut | p_dshi6 >= cut | p_dshi7 >= cut | p_dshi8 >= cut | p_dshi9 >= cut | p_dshi10b >= cut | p_dshi11 >= cut, 1, 0, 0)) %>%
  # val labels
  copy_labels(dsh_raw) %>%
  var_labels(p_dshi_sum = "DSHi total score",
             p_dshi_sc = "DSHi Engaging one time in at least one of the 11 items"
             )

# Histograms
dsh %>% 
  select(starts_with("p_dsh"), -p_dshi10a_1) %>% 
  hist.data.frame()
library(Hmisc)

```

# Export
```{r}
dsh %>% export("bhrc_data/Proc/DSHi.rds")
```

